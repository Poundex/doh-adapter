package net.poundex.dohadapter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.util.Objects;

@SpringBootApplication
@EnableConfigurationProperties(DohAdapterConfig.class)
@Slf4j
public class DohAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DohAdapterApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner configStarter(DohAdapterConfig config) {
		return args -> {
			Objects.requireNonNull(config.getResolvedUpstream(), "Upstream DNS IP (input was '" + config.getUpstream() + "')");
			log.info("Loaded config:\n{}\n", config);
		};
	}
}
