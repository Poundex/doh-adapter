package net.poundex.dohadapter.query

import net.poundex.dohadapter.dns.RecordType
import net.poundex.dohadapter.testutil.AbstractFunctionalSpec
import org.springframework.http.HttpStatus
import org.xbill.DNS.Message
import org.xbill.DNS.Name
import org.xbill.DNS.Record
import org.xbill.DNS.Section

import java.net.http.HttpResponse

class QueryApiFunctionalSpec extends AbstractFunctionalSpec {
	void "GET request for A record for real domain (JSON)"() {
		when: "A query is made via GET request"
		HttpResponse resp = get("/dns-query?name=www.example.com.&type=A", jsonAsMapBodyHandler())

		then: "200 OK is returned"
		resp.statusCode() == HttpStatus.OK.value()

		and: "Response contains successful answer to query"
		with(resp.body()) {
			Status == 0
			!TC
			RD
			RA
			AD 
			! CD
			with(Question) {
				name == "www.example.com."
				type == RecordType.A.value
			}
			with(Answer.first()) {
				name == "www.example.com."
				type == RecordType.A.value
				TTL >= 10_000
				TTL < 100_000
				data == "93.184.216.34"
			}
		}
	}

	void "POST request for A record for real domain (DNS MESSAGE)"() {
		when: "A query is made via POST request"
		HttpResponse resp = postBytes("/dns-query", 
				Message.newQuery(Record.newRecord(
						Name.fromString("www.example.com."), 1, 1))
						.toWire())

		then: "200 OK is returned"
		resp.statusCode() == HttpStatus.OK.value()

		and: "Response contains successful answer to query"
		with(new Message(resp.body())) {
			with(it.getQuestion()) { q ->
				q.getName().toString(false) == "www.example.com."
				q.getType() == RecordType.A.value
			}
			with(it.getSection(Section.ANSWER).first()) { a ->
				a.getName().toString(false) == "www.example.com."
				a.getType() == RecordType.A.value
				a.getTTL() > 5_000
				a.getTTL() <= 100_000
				a.rdataToString() == "93.184.216.34"
			}
		}
	}
}
