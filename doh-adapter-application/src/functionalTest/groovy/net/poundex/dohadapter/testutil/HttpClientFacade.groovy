package net.poundex.dohadapter.testutil

import com.fasterxml.jackson.databind.ObjectMapper
import io.vavr.Lazy

import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.net.http.HttpResponse.BodyHandler

trait HttpClientFacade {

	private final ObjectMapper objectMapper = new ObjectMapper()
	private final HttpClient httpClient = HttpClient.newHttpClient()

	private String httpEndpoint

	void setHttpEndpoint(String httpEndpoint) {
		this.httpEndpoint = httpEndpoint
	}

	public <T> HttpResponse<T> get(String path, BodyHandler<T> bodyHandler) {
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(httpEndpoint).resolve(path))
				.build()
		return httpClient.send(request, bodyHandler)
	}

	HttpResponse<Void> get(String path) {
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(httpEndpoint).resolve(path))
				.build()
		return httpClient.send(request, HttpResponse.BodyHandlers.discarding())
	}

	HttpResponse<Map<String, Object>> post(String path, Map json) {
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(httpEndpoint).resolve(path))
				.POST(HttpRequest.BodyPublishers.ofByteArray(objectMapper.writeValueAsBytes(json)))
				.header("Content-Type", "application/json")
				.build()
		return httpClient.send(request, jsonAsMapBodyHandler())
	}
	
	HttpResponse<byte[]> postBytes(String path, byte[] bytes) {
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(httpEndpoint).resolve(path))
				.POST(HttpRequest.BodyPublishers.ofByteArray(bytes))
				.header("Content-Type", "application/dns-message")
				.build()
		return httpClient.send(request, HttpResponse.BodyHandlers.ofByteArray())
	}

	BodyHandler<Map<String, Object>> jsonAsMapBodyHandler() {
		return (HttpResponse.ResponseInfo responseInfo) -> HttpResponse.BodySubscribers.mapping(
				HttpResponse.BodySubscribers.ofInputStream(),
				is -> Lazy.val(() -> objectMapper.readValue(is, Map), Map))
	}

	BodyHandler<List<Map<String, Object>>> jsonAsListOfMapsBodyHandler() {
		return (HttpResponse.ResponseInfo responseInfo) -> HttpResponse.BodySubscribers.mapping(
				HttpResponse.BodySubscribers.ofInputStream(),
				is -> Lazy.val(() -> objectMapper.readValue(is, List), List))
	}
}
