package net.poundex.dohadapter.dns;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.xbill.DNS.Record;
import org.xbill.DNS.*;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class DnsJavaResolver implements Resolver {
	
	private final SimpleResolver simpleResolver;
	
	@Override
	public Mono<QueryResult> resolve(String name, RecordType type) {
		return Try.of(() -> simpleResolver.sendAsync(
						Message.newQuery(
								Record.newRecord(Name.fromString(name), type.getValue(), DClass.IN))))
				.map(Mono::fromCompletionStage)
				.get()
				.map(msg -> new QueryResult(ResponseCode.fromCode(msg.getRcode()),
						msg.getSectionRRsets(Section.ANSWER).stream()
								.map(rrs -> new QueryResult.Answer(
										rrs.getName().toString(false),
										RecordType.fromCode(rrs.getType()),
										rrs.getTTL(),
										rrs.first().rdataToString()))
								.toList(), msg.toWire()));
	}
}
