package net.poundex.dohadapter.dns;

import io.vavr.control.Try;
import net.poundex.dohadapter.DohAdapterConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xbill.DNS.SimpleResolver;

@Configuration
public class DnsJavaConfiguration {
	
	@Bean
	public SimpleResolver simpleResolver(DohAdapterConfig config) {
		return Try.of(() -> new SimpleResolver(config.getResolvedUpstream())).get();
	}
}
