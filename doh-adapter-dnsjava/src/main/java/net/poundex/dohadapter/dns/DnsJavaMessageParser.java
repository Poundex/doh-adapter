package net.poundex.dohadapter.dns;

import io.vavr.control.Try;
import org.springframework.stereotype.Service;
import org.xbill.DNS.Message;
import reactor.core.publisher.Mono;

@Service
public class DnsJavaMessageParser implements MessageParser {
	@Override
	public Mono<DnsMessage> parse(byte[] bytes) {
		return Mono.fromSupplier(() -> Try.of(() -> new Message(bytes))
				.map(m -> new DnsMessage(
						m.getQuestion().getName().toString(false),
						RecordType.fromCode(m.getQuestion().getType())))
				.get());
	}
}
