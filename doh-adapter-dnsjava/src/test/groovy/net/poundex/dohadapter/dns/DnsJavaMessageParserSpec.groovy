package net.poundex.dohadapter.dns


import org.xbill.DNS.Message
import org.xbill.DNS.Name
import org.xbill.DNS.Record
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class DnsJavaMessageParserSpec extends Specification {
	
	@Subject
	DnsJavaMessageParser parser = new DnsJavaMessageParser()
	
	void "Parse DNS messages"() {
		given:
		byte[] msg = Message.newQuery(Record.newRecord(Name.fromString("www.example.com."), 1, 1)).toWire()

		when:
		Mono<MessageParser.DnsMessage> parsed = parser.parse(msg)
		
		then:
		parsed.isJust {
			with(it) { p ->
				p.name() == "www.example.com."
				p.recordType() == RecordType.A
			}
		}
	}
}
