package net.poundex.dohadapter.dns

import org.xbill.DNS.ARecord
import org.xbill.DNS.Message
import org.xbill.DNS.Name
import org.xbill.DNS.Section
import org.xbill.DNS.SimpleResolver
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.util.concurrent.CompletableFuture

class DnsJavaResolverSpec extends Specification {
	
	SimpleResolver simpleResolver = Stub()
	
	@Subject
	DnsJavaResolver resolver = new DnsJavaResolver(simpleResolver)
	
	void "Resolve query and return QueryResult"() {
		given:
		simpleResolver.sendAsync({ Message m -> m.getQuestion().getName().toString(false) == "www.example.com."}) >>
				CompletableFuture.completedStage(new Message().tap {
					addRecord(ARecord.fromString(Name.fromString("www.example.com."), RecordType.A.value, 1, 30, "1.2.3.4", Name.root), Section.ANSWER)
				})
		
		when:
		Mono<QueryResult> qr = resolver.resolve("www.example.com.", RecordType.A)
		
		then:
		qr.isJust { r ->
			with(r) { rr ->
				rr.responseCode() == ResponseCode.NoError
				rr.answers().contains(new QueryResult.Answer("www.example.com.", RecordType.A, 30, "1.2.3.4"))
			}
		}
	}
	
}
