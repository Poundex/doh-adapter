package net.poundex.dohadapter.query

import net.poundex.dohadapter.dns.MessageParser
import net.poundex.dohadapter.dns.QueryResult
import net.poundex.dohadapter.dns.RecordType
import net.poundex.dohadapter.dns.Resolver
import net.poundex.dohadapter.dns.ResponseCode
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class QueryControllerSpec extends Specification {
	
	Resolver resolver = Stub()
	MessageParser messageParser = Stub()
	
	@Subject
	QueryController queryController = new QueryController(resolver, messageParser)
	
	void "Resolve requested dns json query"() {
		given:
		resolver.resolve("www.example.com.", RecordType.A) >> 
				Mono.just(new QueryResult(ResponseCode.NoError, [
						new QueryResult.Answer("www.example.com.", RecordType.A, 30, "1.2.3.4")
				]))
		
		when:
		Mono<ResponseEntity<QueryResponse>> r = queryController.dnsQuery("www.example.com.", RecordType.A)
		
		then:
		r.isJust {
			with(it) { resp ->
				resp.getStatusCode() == HttpStatus.OK
				with(resp.body) {
					Status() == ResponseCode.NoError.value
					with(Question()) {
						name() == "www.example.com."
						type() == 1
					}
					Answer().contains(new QueryResponse.Answer(
							"www.example.com.", 
							1, 
							30, 
							"1.2.3.4"))
				}
			}
		}
		
	}

	void "Resolve requested dns message query"() {
		given:
		byte[] question = [1, 2, 3, 4] as byte[]
		byte[] answer = [4, 3, 2, 1] as byte[]
		messageParser.parse(question) >> Mono.just(new MessageParser.DnsMessage("www.example.com.", RecordType.A))
		resolver.resolve("www.example.com.", RecordType.A) >>
				Mono.just(new QueryResult(ResponseCode.NoError, [
						new QueryResult.Answer("www.example.com.", RecordType.A, 30, "1.2.3.4")
				], answer))

		when:
		Mono<ResponseEntity<byte[]>> r = queryController.dnsQuery(question)

		then:
		r.isJust {
			with(it) { resp ->
				resp.getStatusCode() == HttpStatus.OK
				resp.getBody() == answer
			}
		}

	}
}
