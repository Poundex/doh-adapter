package net.poundex.dohadapter.query;

import java.util.List;

public record QueryResponse(
		int Status,
		boolean TC,
		boolean RD,
		boolean RA,
		boolean AD,
		boolean CD,
		Question Question,
		List<Answer> Answer,
		String comment
) {
	public record Question(String name, int type) { }
	public record Answer(String name, int type, long TTL, String data) { }
	
}
