package net.poundex.dohadapter.query;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.dohadapter.dns.MessageParser;
import net.poundex.dohadapter.dns.QueryResult;
import net.poundex.dohadapter.dns.RecordType;
import net.poundex.dohadapter.dns.Resolver;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/dns-query")
@RequiredArgsConstructor
@Slf4j
public class QueryController {
	
	private final Resolver resolver;
	private final MessageParser messageParser;
	
	@GetMapping(produces = {"application/dns-json", "application/dns+json"})
	public Mono<ResponseEntity<QueryResponse>> dnsQuery(@RequestParam("name") String name, @RequestParam("type") RecordType type) {
		return resolver.resolve(name, type)
				.map(qr -> resultToResponse(name, type, qr))
//				.doOnNext(qr -> log.info("Q {}({}) ==> {}", type, name, qr))
				.map(ResponseEntity::ok);
	}
	
	@PostMapping(consumes = "application/dns-message")
	public Mono<ResponseEntity<byte[]>> dnsQuery(@RequestBody byte[] dnsMessage) {
		return messageParser.parse(dnsMessage)
				.flatMap(m -> resolver.resolve(m.name(), m.recordType()))
//				.doOnNext(qr -> log.info("Q {}({}) ==> {}", type, name, qr))
				.map(QueryResult::raw)
				.map(ResponseEntity::ok);
	}
	
	private static QueryResponse resultToResponse(String name, RecordType type, QueryResult qr) {
		return new QueryResponse(
				qr.responseCode().getValue(),
				false,
				true,
				true,
				true,
				false,
				new QueryResponse.Question(name, type.getValue()),
				qr.answers().stream().map(a -> new QueryResponse.Answer(
						a.name(),
						a.type().getValue(),
						a.ttl(),
						a.data())).toList(),
				"");
	}
}
