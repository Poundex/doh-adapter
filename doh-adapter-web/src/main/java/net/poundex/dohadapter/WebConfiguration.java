package net.poundex.dohadapter;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.codec.EncoderHttpMessageWriter;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.util.MimeType;
import org.springframework.web.reactive.config.WebFluxConfigurer;

import java.util.stream.Stream;

@Configuration
public class WebConfiguration implements WebFluxConfigurer {
	@Override
	public void configureHttpMessageCodecs(ServerCodecConfigurer configurer) {
		configurer.getWriters().stream().filter(w -> w instanceof EncoderHttpMessageWriter<?>)
				.map(w -> (EncoderHttpMessageWriter<?>) w)
				.map(EncoderHttpMessageWriter::getEncoder)
				.filter(en -> en instanceof Jackson2JsonEncoder)
				.map(en -> (Jackson2JsonEncoder) en)
				.findFirst()
				.ifPresent(e -> configurer.defaultCodecs().jackson2JsonEncoder(
						new Jackson2JsonEncoder(e.getObjectMapper(),
								Stream.concat(
												e.getEncodableMimeTypes().stream(),
												Stream.of(new MediaType("application", "dns-json")))
										.toArray(MimeType[]::new))));
	}
}
