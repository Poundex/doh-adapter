package net.poundex.dohadapter.test

import groovy.transform.stc.ClosureParams
import groovy.transform.stc.FirstParam
import reactor.core.publisher.Mono
import reactor.test.StepVerifier

import java.time.Duration

class AssertablePublisherExtension {

	static <T> void isJust(Mono<T> self, @ClosureParams(value = FirstParam.FirstGenericType) Closure<?> closure) {
		StepVerifier
				.create(self)
				.assertNext(next -> closure.call(next))
				.expectComplete()
				.verify(Duration.ofSeconds(1))
	}
}