package net.poundex.dohadapter.dns;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public enum ResponseCode {
	
	NoError(0),
	FormErr(1),
	ServFail(2),
	NXDomain(3),
	NotImp(4),
	Refused(5);
	
	@Getter
	private final int value;
	
	private final static Map<Integer, ResponseCode> FROM_CODE;
	static {
		FROM_CODE = Arrays.stream(ResponseCode.values())
				.collect(Collectors.toMap(ResponseCode::getValue, rc -> rc));
	}

	public static ResponseCode fromCode(int code) {
		return FROM_CODE.get(code);
	}
}
