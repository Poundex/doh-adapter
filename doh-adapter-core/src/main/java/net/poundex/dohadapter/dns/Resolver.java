package net.poundex.dohadapter.dns;

import reactor.core.publisher.Mono;

public interface Resolver {
	Mono<QueryResult> resolve(String name, RecordType type);
}
