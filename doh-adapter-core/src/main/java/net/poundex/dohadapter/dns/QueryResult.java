package net.poundex.dohadapter.dns;

import java.util.List;

public record QueryResult(ResponseCode responseCode, List<Answer> answers, byte[] raw) {
	public record Answer(String name, RecordType type, long ttl, String data) { }
}
