package net.poundex.dohadapter.dns;

import reactor.core.publisher.Mono;

public interface MessageParser {
	record DnsMessage(String name, RecordType recordType) { }
	
	Mono<DnsMessage> parse(byte[] bytes);
}
