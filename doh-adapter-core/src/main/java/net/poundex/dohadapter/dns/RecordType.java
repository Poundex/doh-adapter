package net.poundex.dohadapter.dns;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public enum RecordType {
	
	A(1),
	NS(2),
	CNAME(5),
	SOA(6),
	MX(15),
	TXT(16),
	AAAA(28),
	SRV(33);
	
	@Getter
	private final int value;

	private final static Map<Integer, RecordType> FROM_CODE;
	static {
		FROM_CODE = Arrays.stream(RecordType.values())
				.collect(Collectors.toMap(RecordType::getValue, rc -> rc));
	}

	public static RecordType fromCode(int code) {
		return FROM_CODE.get(code);
	}
}
