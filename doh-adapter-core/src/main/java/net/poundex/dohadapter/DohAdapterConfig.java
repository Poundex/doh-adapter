package net.poundex.dohadapter;

import io.vavr.Lazy;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@ConfigurationProperties("dns")
@NoArgsConstructor
public class DohAdapterConfig {
	
	@Setter
	@Getter
	private String upstream;
	
	private final Lazy<String> resolvedUpstream = Lazy.of(this::doGetResolvedUpstream);
	
	public String getResolvedUpstream() {
		return resolvedUpstream.get();
	}
	
	private String doGetResolvedUpstream() {
		if(upstream.startsWith("$"))
			return System.getenv(upstream.substring(1));
		return upstream;
	}

	@Override
	public String toString() {
		return Map.of(
				"upstream", getResolvedUpstream()
		).toString();
	}
}
