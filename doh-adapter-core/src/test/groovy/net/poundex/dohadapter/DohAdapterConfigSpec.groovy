package net.poundex.dohadapter

import spock.lang.Specification

class DohAdapterConfigSpec extends Specification {
	void "Return static upstream"() {
		given:
		DohAdapterConfig config = new DohAdapterConfig()
		config.setUpstream("1.2.3.4")
		
		expect:
		config.resolvedUpstream == "1.2.3.4"
	}
	
	void "Return env upstream"() { 
		given:
		DohAdapterConfig config = new DohAdapterConfig()
		config.setUpstream('$HOME')

		expect:
		config.resolvedUpstream.contains("/")
	}
}
