package net.poundex.dohadapter.dns

import spock.lang.Specification

class RecordTypeSpec extends Specification {
	void "Lookup by code"() {
		expect:
		RecordType.fromCode(code) == rc

		where:
		code  || rc
		1     || RecordType.A
		5     || RecordType.CNAME
		16    || RecordType.TXT
	}
}
