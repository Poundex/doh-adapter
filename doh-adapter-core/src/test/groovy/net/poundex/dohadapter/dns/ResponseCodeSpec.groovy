package net.poundex.dohadapter.dns

import spock.lang.Specification

class ResponseCodeSpec extends Specification {
	void "Lookup by code"() {
		expect:
		ResponseCode.fromCode(code) == rc

		where:
		code || rc
		0    || ResponseCode.NoError
		2    || ResponseCode.ServFail
		3    || ResponseCode.NXDomain
	}
}
